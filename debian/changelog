liblog-dispatch-perl-perl (0.05-1) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ intrigeri ]
  * Import new upstream release.
    Thanks a lot to Karen Etheridge for adopting upstream maintenance!
  * Don't install upstream TODO: it's now excluded from the release tarballs
  * Install upstream CONTRIBUTING file
  * Point upstream contact to the new upstream maintainer
  * Declare compliance with Debian Policy 4.5.0
  * Bump debhelper compatibility level to 13
  * Annotate test-only build-dependency with <!nocheck>
  * Explicitly declare libtest-simple-perl test-only build-dependency
    (provided by perl)
  * Add debian/upstream/metadata
  * Set the Uploaders control field, as needed despite team-maintenance
  * Set Rules-Requires-Root: no
  * Improve package descriptions

 -- intrigeri <intrigeri@debian.org>  Mon, 18 May 2020 15:25:08 +0000

liblog-dispatch-perl-perl (0.04-2) unstable; urgency=low

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Don't install boilerplate README anymore.
  * Clean up (build) dependencies. Drop unneeded version constraints,
    remove perl-modules.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.8.
  * Bump debhelper compatibility level to 9.
  * Improve short description.

 -- gregor herrmann <gregoa@debian.org>  Sat, 16 Apr 2016 16:48:43 +0200

liblog-dispatch-perl-perl (0.04-1) unstable; urgency=low

  * Initial Release. closes: #681324.

 -- Dmitry E. Oboukhov <unera@debian.org>  Thu, 12 Jul 2012 14:06:09 +0400
